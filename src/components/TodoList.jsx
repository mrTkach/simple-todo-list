import React from 'react';
import Todo from './Todo';

const TodoList = ({  todos, filteredTodos, setTodos }) => {
	return (
		<div className="todo-container">
			<ul className="todo-list">
				{filteredTodos.map( item =>  <Todo
					text={item.text}
					key={item.id}
					id={item.id}
					completed={item.completed}
					todos={todos}
					setTodos={setTodos}
				/> )}
			</ul>
		</div>
	)
}

export default TodoList;
